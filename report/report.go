package report

// Report is the interface common to different reporters
type Report interface {
	Info(name string, line string)
}
