package report

import (
	"fmt"
	"io"
	"strings"
	"sync"
)

var (
	ansiReset  = "\033[0m"
	ansiColors = []string{"\033[32m", "\033[33m", "\033[34m", "\033[35m", "\033[36m"}
)

// Terminal implements report methods that go to a terminal
type Terminal struct {
	headWidth  int
	headPad    string
	out        io.Writer
	colorsLock sync.Mutex
	colors     map[string]string
	colorIndex int
}

// NewTerminal initialises a terminal reporter using given writer as target
// uses ANSI
func NewTerminal(headWidth int, out io.Writer) Report {
	return &Terminal{
		headWidth:  headWidth,
		headPad:    strings.Repeat(" ", headWidth) + "| ",
		out:        out,
		colorsLock: sync.Mutex{},
		colors:     map[string]string{},
	}
}

// Info logs a plain info string from an action (stdout usually)
func (t *Terminal) Info(name string, line string) {
	for len(name) < t.headWidth {
		name += " "
	}

	t.colorsLock.Lock()
	col, found := t.colors[name]
	if !found {
		t.colorIndex = (t.colorIndex + 1) % len(ansiColors)
		col = ansiColors[t.colorIndex]
		t.colors[name] = col
	}
	t.colorsLock.Unlock()

	fmt.Fprint(t.out, col+name+"| "+ansiReset)
	line = strings.TrimRight(line, "\r\n\t ")
	lines := strings.Split(line, "\n")
	fmt.Fprintln(t.out, lines[0])
	for _, nl := range lines[1:] {
		fmt.Fprintln(t.out, col+t.headPad+ansiReset+nl)
	}
}
