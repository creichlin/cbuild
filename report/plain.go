package report

import (
	"fmt"
	"io"
	"strings"
)

// Plain implements a report that writes pipe separated lines with no decoration
type Plain struct {
	out io.Writer
}

// NewPlain initialises a plain reporter using given writer as target
func NewPlain(out io.Writer) Report {
	return &Plain{
		out: out,
	}
}

// Info logs a plain info string from an action (stdout usually)
func (t *Plain) Info(name string, line string) {
	fmt.Fprint(t.out, name+" | ")
	line = strings.TrimRight(line, "\r\n\t ")
	lines := strings.Split(line, "\n")
	fmt.Fprintln(t.out, lines[0])
	for _, nl := range lines[1:] {
		fmt.Fprintln(t.out, strings.Repeat(" ", len(name))+" | "+nl)
	}
}
