package report

import (
	"bytes"
	"fmt"
	"testing"
)

func TestBasic(t *testing.T) {
	out := bytes.NewBufferString("")
	term := NewTerminal(10, out)
	term.Info("foo", "one line")
	term.Info("bar", "one line with newline\n")
	term.Info("bar", "one line with newline and space  \n     ")
	term.Info("boo", "multiple lines\nsecond\nthird")
	term.Info("testxxxx", "multiple lines\nsecond\nthird")
	term.Info("testyyyyy", "multiple lines\nsecond\nthird")
	term.Info("a", "multiple")
	fmt.Println(out.String())
}
