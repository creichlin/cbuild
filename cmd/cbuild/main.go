package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/runtime"
)

var dec string
var logging bool

func main() {
	flag.StringVar(&dec, "declaration", "cbuild.yaml", "path to the declaration file")
	flag.BoolVar(&logging, "log", false, "enable logging")
	flag.Parse()

	decgo, err := declaration.ReadDeclarationFile(dec)
	if err != nil {
		log.Fatal(err)
	}

	r, err := runtime.New(decgo, decgo.Exclude, runtime.OptLogging(logging))
	if err != nil {
		log.Fatal(err)
	}

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt)

	<-exit

	r.Close()

	time.Sleep(time.Millisecond * 10)
}
