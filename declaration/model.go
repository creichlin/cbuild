package declaration

import (
	"encoding/json"
)

// CBuild is the root element of a cbuild declaration file
type CBuild struct {
	Operations map[string]Operation `json:"operations"`
	Exclude    []string             `json:"exclude,omitempty"`
}

// UnmarshalJSON needs to be implemented to populate the Operations interface
// with the correct operation type
func (c *CBuild) UnmarshalJSON(b []byte) error {
	objMap := map[string]*json.RawMessage{}
	err := json.Unmarshal(b, &objMap)
	if err != nil {
		return err
	}

	if ops, has := objMap["operations"]; has {
		c.Operations = map[string]Operation{}

		opsMap := map[string]*json.RawMessage{}
		err := json.Unmarshal(*ops, &opsMap)
		if err != nil {
			return err
		}

		for key, opData := range opsMap {
			opMap := map[string]interface{}{}
			err := json.Unmarshal(*opData, &opMap)
			if err != nil {
				return err
			}

			t, err := buildType(opMap["type"], opData)
			if err != nil {
				return err
			}
			c.Operations[key] = t
		}
	}

	if exi, has := objMap["exclude"]; has {
		c.Exclude = []string{}
		err := json.Unmarshal(*exi, &c.Exclude)
		if err != nil {
			return err
		}
	}
	return nil
}

// buildType will unmarshal json into struct given by type
func buildType(t interface{}, message *json.RawMessage) (Operation, error) {
	switch t {
	case "fs-trigger":
		fst := &FSTrigger{}
		err := json.Unmarshal(*message, fst)
		return fst, err
	case "log":
		la := &LogAction{}
		err := json.Unmarshal(*message, la)
		return la, err
	case "exec":
		ex := &ExecAction{}
		err := json.Unmarshal(*message, ex)
		return ex, err
	case "service":
		sx := &ServiceAction{}
		err := json.Unmarshal(*message, sx)
		return sx, err
	case "multi":
		sx := &MultiAction{}
		err := json.Unmarshal(*message, sx)
		return sx, err
	case "restart":
		r := &RestartAction{}
		err := json.Unmarshal(*message, r)
		return r, err
	case "throttle":
		r := &ThrottleOperation{}
		err := json.Unmarshal(*message, r)
		return r, err
	default:
		panic("operation must have valid type")
	}
}

// Visit will apply the visitor to all operations
func (c *CBuild) Visit(visitor OperationVisitor) error {
	for name, op := range c.Operations {
		err := op.Accept(name, visitor)
		if err != nil {
			return err
		}
	}
	return nil
}

// Operation is the common interface for all operations, actions and triggers
type Operation interface {
	Accept(string, OperationVisitor) error
}

// OperationVisitor allows to perform actions on each Operation type
type OperationVisitor interface {
	FSTrigger(string, *FSTrigger) error
	LogAction(string, *LogAction) error
	ExecAction(string, *ExecAction) error
	ServiceAction(string, *ServiceAction) error
	MultiAction(string, *MultiAction) error
	RestartAction(string, *RestartAction) error
	Throttle(string, *ThrottleOperation) error
}

// FSTrigger is a filesystem trigger declaration
type FSTrigger struct {
	Type       string            `json:"type"`
	Pattern    string            `json:"pattern"`
	Action     string            `json:"action"`
	Parameters map[string]string `json:"parameters,omitempty"`
}

// Accept routes to the proper visitor method
func (f *FSTrigger) Accept(name string, v OperationVisitor) error {
	return v.FSTrigger(name, f)
}

// LogAction is a declaration for logging something to stdout
type LogAction struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

// Accept routes to the proper visitor method
func (l *LogAction) Accept(name string, v OperationVisitor) error {
	return v.LogAction(name, l)
}

// ExecAction is a declaration for executing a command
type ExecAction struct {
	Type      string   `json:"type"`
	Command   string   `json:"command"`
	Args      []string `json:"args"`
	StdinFrom string   `json:"stdin-from,omitempty"`
	StdoutTo  string   `json:"stdout-to,omitempty"`
}

// Accept routes to the proper visitor method
func (x *ExecAction) Accept(name string, v OperationVisitor) error {
	return v.ExecAction(name, x)
}

// ServiceAction is a declaration for executing/restarting a service
type ServiceAction struct {
	Type    string            `json:"type"`
	Command string            `json:"command"`
	Args    []string          `json:"args"`
	Env     map[string]string `json:"env"`
}

// Accept routes to the proper visitor method
func (s *ServiceAction) Accept(name string, v OperationVisitor) error {
	return v.ServiceAction(name, s)
}

// MultiAction is the declaration for triggering multiple actions
type MultiAction struct {
	Type    string   `json:"type"`
	Actions []string `json:"actions"`
}

// Accept routes to the proper visitor method
func (s *MultiAction) Accept(name string, v OperationVisitor) error {
	return v.MultiAction(name, s)
}

// RestartAction is the declaration for the restart action
type RestartAction struct {
	Type string `json:"type"`
}

// Accept routes to the proper visitor method
func (r *RestartAction) Accept(name string, v OperationVisitor) error {
	return v.RestartAction(name, r)
}

// ThrottleOperation is a declaration for throttling a trigger
type ThrottleOperation struct {
	Type   string `json:"type"`
	Delay  int    `json:"delay"`
	Action string `json:"action"`
}

// Accept routes to the proper visitor method
func (x *ThrottleOperation) Accept(name string, v OperationVisitor) error {
	return v.Throttle(name, x)
}
