package declaration

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/creichlin/goschema"
	"github.com/ghodss/yaml"
)

type test struct {
	Name string      `json:"name"`
	Body interface{} `json:"body"`
}

func TestReadDeclaration(t *testing.T) {
	d, err := ioutil.ReadFile("test.yaml")
	if err != nil {
		t.Fatal(err)
	}
	tests := []test{}
	err = yaml.Unmarshal(d, &tests)
	if err != nil {
		t.Fatal(err)
	}

	for _, tc := range tests {
		t.Run(tc.Name, func(t *testing.T) {
			y, err := yaml.Marshal(tc.Body)
			if err != nil {
				t.Fatal(err)
			}
			dec, err := ReadDeclarationString(string(y))
			if err != nil {
				t.Error(err)
				return
			}
			actual, err := yaml.Marshal(dec)
			if err != nil {
				t.Error(err)
				return
			}
			if string(y) != string(actual) {
				t.Errorf("input and output are different\nin :\n%v\nout:\n%v", string(y), string(actual))
			}
		})
	}
}

func TestDoc(t *testing.T) {
	t.Skip()
	fmt.Println(goschema.Doc(schema))
}
