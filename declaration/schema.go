package declaration

import (
	"io/ioutil"

	"github.com/creichlin/goschema"
	"github.com/ghodss/yaml"
)

var schema = goschema.NewObjectType("CBuild declaration", func(p goschema.ObjectType) {
	p.Attribute("operations").Map(func(p goschema.MapType) {
		p.SomeOf(func(p goschema.SomeOf) {
			// fs trigger
			p.Object("fs-trigger declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("fs-trigger", "type")
				p.Attribute("pattern").String(
					"is a regular expression that matches a filename of a changed file")
				p.Attribute("action").String(
					"is the name of the action that should be executed")
				p.Optional("parameters").Map(func(p goschema.MapType) {
					p.String("value is a go template using pattern groups as model")
				})
			})

			// log action
			p.Object("log declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("log", "type")
				p.Attribute("message").String(
					"is a template that gets evaluated using gotemplate and parameters as input")
			})

			// exec action
			p.Object("exec declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("exec", "type")
				p.Attribute("command").String(
					"is the name of a n executable")
				p.Attribute("args").List(func(p goschema.ListType) {
					p.String("each element is a parameter to the executable")
				})
				p.Optional("stdin-from").String("read stdin from given file, is evaluated as a template")
				p.Optional("stdout-to").String("write stdout to given file, is evaluated as a template")
			})

			// service action
			p.Object("service declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("service", "type")
				p.Attribute("command").String(
					"is the name of a n executable")
				p.Optional("args").List(func(p goschema.ListType) {
					p.String("each element is a parameter to the executable")
				})
				p.Optional("env").Map(func(p goschema.MapType) {
					p.String("env variables")
				})
			})

			// multi action
			p.Object("multi declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("multi", "type")
				p.Attribute("actions").List(func(p goschema.ListType) {
					p.String("all the actions that should be triggered")
				})
			})

			// restart action
			p.Object("restart declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("restart", "type")
			})

			// throttle operation
			p.Object("throttle declaration", func(p goschema.ObjectType) {
				p.Attribute("type").Enum("specifies").Add("throttle", "type")
				p.Attribute("delay").Int("How long to wait till the action gets triggered")
				p.Attribute("action").String(
					"is the name of the action that should be executed")
			})
		})
	})

	p.Optional("exclude").List(func(p goschema.ListType) {
		p.String("a list of folder patterns to exclude from watching (.git, **/node_modules)")
	})
	p.Optional("exclude-on-startup").List(func(p goschema.ListType) {
		p.String("do not break when using older version (has no effect, slows down the whole thing)")
	})
})

// ReadDeclarationString interprets the string as yaml cbuild declaration and tries to read it into
// the internal model structure
func ReadDeclarationString(declaration string) (*CBuild, error) {
	t := interface{}(nil)
	err := yaml.Unmarshal([]byte(declaration), &t)
	if err != nil {
		return nil, err
	}
	errc := goschema.ValidateGO(schema, t)
	if errc.Has() {
		return nil, errc
	}

	cb := &CBuild{}
	err = yaml.Unmarshal([]byte(declaration), cb)
	if err != nil {
		return nil, err
	}
	return cb, nil
}

// ReadDeclarationFile reads and parses the yaml declaration from provided file path
func ReadDeclarationFile(file string) (*CBuild, error) {
	d, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	return ReadDeclarationString(string(d))
}

// MustReadDeclarationFile reads and parses the yaml declaration from provided file path
// and panics if an error occurs
func MustReadDeclarationFile(file string) *CBuild {
	c, err := ReadDeclarationFile(file)
	if err != nil {
		panic(err)
	}
	return c
}
