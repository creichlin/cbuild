Local continuous build
======================

The very simple usecase is: watch a file, when it changes, do some actions.

This is so far only a proof of concept.

Progress
========

First iteration
---------------

### Target

Just read a yaml file with path patterns. If one of those paths/files
have a change, print a log.

Paths are regular expressions with groups. The log task will print a text
that can interpolate the matched path/groups.

### Learned
  - IDEs are slow/weird, so one file save might trigger a few times in a second -> explicit throttling for certain files
  - Usecase for inplace changes (gofmt) can not be done reliably, endless re-trigger on file write -> filehash cache
  - Separation between triggers and actions not viable (throttling, hash-cache) -> triggers/actions should be aspects
    of entity

Second iteration
----------------

### Target

Add exec action which calls an executable, allows for using cbuild for itself. Adding sys action which can
reload cbuild on trigger (cbuild.yaml or executable: for faster dev cycle).

### Learned

Reloading of cbuild executable is difficult using only one process. Putting
new process to foreground is not transparent for os or even shell it seems.
The approach of starting the executable twice, one as a process restarter
and the other as the real process should work on more os' and have less
fragile parts.

Works quite well so far but is a bit clumsy to use:

- Triggering multiple actions from one action would make it easier.
This could be implemented as a preprocessor using an action splitter to keep the core small.

- Regexp pattern are difficult to get right, maybe a different syntax which
is easier for file matches might be helpful. Maybe also preprocessor.

Third iteration
---------------

### Target

Executable should optionally be able to read stdin from file and write stdout to file.
Included should be creation of missing folders for output. Only
writing the output file when it really changed, so not to many
actions are triggered unnecessarily.

### Learned

Works pretty well for integration with common linux tools.

Fourth iteration
----------------

### Target

- Throttling of actions

### Learned

- Not to hard to implement after quite a few months of absence
- Output is much less cluttered and works pretty well
- cbuild.yaml files get quite complicated for simpel usecases
