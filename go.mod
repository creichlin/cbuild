module gitlab.com/creichlin/cbuild

go 1.13

require (
	github.com/creichlin/goschema v0.0.0-20170919205605-c2a1e61f6185
	github.com/creichlin/gutil v0.0.0-20170720202424-7e90bad1fd27 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v0.0.0-20180207214316-8bcffc811467 // indirect
	gitlab.com/akabio/rnotify v0.3.3
)
