package runtime

import (
	"fmt"
	"log"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/matcher"
	"gitlab.com/creichlin/cbuild/report"
)

// cBuild is the runtime root instance
// it's a container for the listeners and actions and manages
// them during runtime
type cBuild struct {
	// sources and targets
	stdout      report.Report
	fileChanges chan string
	matcher     *matcher.Matcher
	// internal
	exclude []string
	options []Option
	// declared runtime objects
	operations map[string]operation
	log        func(msg string, p ...interface{})
}

// Runtime exposes functions to stop the runtime
type Runtime interface {
	Close()
	Send(string)
}

// operation is the internal operation interface
type operation interface {
	run(params map[string]string)
	close()
}

// New constructs a new Runtime instance using provided options
func New(d *declaration.CBuild, exclude []string, options ...Option) (Runtime, error) {
	c := &cBuild{
		operations: map[string]operation{},
		log:        func(string, ...interface{}) {},
		exclude:    exclude,
	}

	err := c.applyOptions(options)
	if err != nil {
		return nil, err
	}

	c.matcher = matcher.NewMatcher(c.fileChanges, c.log)

	err = d.Visit(&buildOpVisitor{c: c})
	if err != nil {
		return nil, err
	}

	err = d.Visit(&buildPostOpVisitor{c: c})
	if err != nil {
		return nil, err
	}

	c.matcher.Start()

	return c, nil
}

// Send will initiate a trigger event using given path
func (c *cBuild) Send(path string) {
	c.fileChanges <- path
}

// Close will close all operations as well as all options.
// Errors are logged but they won't interrupt.
func (c *cBuild) Close() {
	for _, t := range c.operations {
		t.close()
	}
	for _, o := range c.options {
		err := o.close()
		if err != nil {
			log.Printf("Error while closing option, %v", err)
		}
	}
}

type buildOpVisitor struct {
	c *cBuild
}

// FSTrigger will create and register an fs trigger
func (bop *buildOpVisitor) FSTrigger(name string, fs *declaration.FSTrigger) error {
	fst, err := newFsTrigger(bop.c, fs, name, bop.c.log)
	if err != nil {
		return err
	}
	bop.c.operations[name] = fst
	return nil
}

// LogAction will create and register a log action
func (bop *buildOpVisitor) LogAction(name string, l *declaration.LogAction) error {
	log, err := newLogger(bop.c, name, l.Message)
	if err != nil {
		return err
	}
	bop.c.operations[name] = log
	return nil
}

// ExecAction will create and register a exec action
func (bop *buildOpVisitor) ExecAction(name string, l *declaration.ExecAction) error {
	x, err := newExec(bop.c, name, l)
	if err != nil {
		return err
	}
	bop.c.operations[name] = x
	return nil
}

// ServiceAction will create and register a service action
func (bop *buildOpVisitor) ServiceAction(name string, l *declaration.ServiceAction) error {
	x, err := newService(bop.c, name, l)
	if err != nil {
		return err
	}
	bop.c.operations[name] = x
	return nil
}

// MultiAction will create and register a multi action
func (bop *buildOpVisitor) MultiAction(name string, m *declaration.MultiAction) error {
	x := newMulti(bop.c, name, m)
	bop.c.operations[name] = x
	return nil
}

// RestartAction will create and register a restart action
func (bop *buildOpVisitor) RestartAction(name string, r *declaration.RestartAction) error {
	ra := &restartAction{
		name: name,
		c:    bop.c,
	}
	bop.c.operations[name] = ra
	return nil
}

// Throttle will create and register a throttle operation
func (bop *buildOpVisitor) Throttle(name string, dec *declaration.ThrottleOperation) error {
	to := newThrottle(bop.c, dec, name)

	bop.c.operations[name] = to
	return nil
}

type buildPostOpVisitor struct {
	c *cBuild
}

// FSTrigger will link action to itself
func (bop *buildPostOpVisitor) FSTrigger(name string, fs *declaration.FSTrigger) error {
	fst := bop.c.operations[name].(*fsTrigger)

	target, found := bop.c.operations[fst.dec.Action]
	if !found {
		return fmt.Errorf("Could not find target action %v", fst.dec.Action)
	}
	fst.action = target

	return nil
}

// LogAction post op is empty
func (bop *buildPostOpVisitor) LogAction(name string, l *declaration.LogAction) error {
	return nil
}

// ExecAction post op is empty
func (bop *buildPostOpVisitor) ExecAction(name string, l *declaration.ExecAction) error {
	return nil
}

// ServiceAction post op is empty
func (bop *buildPostOpVisitor) ServiceAction(name string, l *declaration.ServiceAction) error {
	return nil
}

// MultiAction post op
func (bop *buildPostOpVisitor) MultiAction(name string, m *declaration.MultiAction) error {
	myself := bop.c.operations[name].(*multi)

	for _, action := range m.Actions {
		target, found := bop.c.operations[action]
		if !found {
			return fmt.Errorf("Could not find target action %v", action)
		}
		myself.actions = append(myself.actions, target)
	}
	return nil
}

// RestartAction post op is empty
func (bop *buildPostOpVisitor) RestartAction(name string, _ *declaration.RestartAction) error {
	return nil
}

// Throttle will link action to itself
func (bop *buildPostOpVisitor) Throttle(name string, t *declaration.ThrottleOperation) error {
	trg := bop.c.operations[name].(*throttle)
	target, found := bop.c.operations[trg.dec.Action]
	if !found {
		return fmt.Errorf("Could not find target action %v", trg.dec.Action)
	}
	trg.action = target

	return nil
}
