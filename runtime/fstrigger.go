package runtime

import (
	"bytes"
	"log"
	"text/template"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/matcher"
)

type fsTrigger struct {
	name      string
	action    operation
	c         *cBuild
	in        chan matcher.Match
	dec       *declaration.FSTrigger
	closeChan chan int
	log       func(msg string, p ...interface{})
}

func newFsTrigger(c *cBuild, dec *declaration.FSTrigger, name string, log func(msg string, p ...interface{})) (*fsTrigger, error) {
	fst := &fsTrigger{
		name:      name,
		c:         c,
		in:        make(chan matcher.Match, 10),
		dec:       dec,
		closeChan: make(chan int, 1),
		log:       log,
	}

	err := c.matcher.Add(dec.Pattern, fst.in)
	if err != nil {
		return nil, err
	}

	go fst.process()

	return fst, nil
}

func (f *fsTrigger) process() {
	for {
		select {
		case <-f.closeChan:
			return
		case m := <-f.in:
			params := map[string]string{}

			if f.dec.Parameters != nil {
				for k, v := range f.dec.Parameters {
					t, err := template.New(k).Parse(v)
					if err != nil {
						log.Fatalf("Can not parse fs trigger parameter template '%v', %v", v, err)
					}
					vEval := bytes.NewBufferString("")
					err = t.Execute(vEval, m.Groups)
					if err != nil {
						log.Fatalf("Can not parse fs trigger parameter template '%v', %v", v, err)
					}

					params[k] = vEval.String()
				}
			}

			f.log("trigger %v %v", f.name, params)

			f.action.run(params)
		}
	}
}

func (f *fsTrigger) close() {
	f.closeChan <- 1
}

func (f *fsTrigger) run(map[string]string) {
	log.Fatal(f.c.stdout, "Can not use fs-trigger %v as target action", f.name)
}
