package runtime

import (
	"os"
	osexec "os/exec"
	"syscall"
)

type restartAction struct {
	name string
	c    *cBuild
}

func (e *restartAction) run(params map[string]string) {
	e.c.stdout.Info(e.name, "restart myself...")

	e.c.Close()
	err := forkMyself()
	if err != nil {
		e.c.stdout.Info(e.name, "failed to restart myself, will exit anyways\n"+err.Error())
	}
}

func (e *restartAction) close() {
	// we don't have any external resources open, so we do nothing
}

func forkMyself() error {
	argv0, err := osexec.LookPath(os.Args[0])
	if nil != err {
		return err
	}
	wd, err := os.Getwd()
	if nil != err {
		return err
	}
	_, err = os.StartProcess(argv0, os.Args, &os.ProcAttr{
		Dir:   wd,
		Env:   os.Environ(),
		Files: []*os.File{os.Stdin, os.Stdout, os.Stderr},
		Sys:   &syscall.SysProcAttr{Pdeathsig: syscall.SIGINT},
	})
	if nil != err {
		return err
	}
	return nil
}
