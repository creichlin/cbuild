package runtime

import (
	"time"

	"gitlab.com/creichlin/cbuild/declaration"
)

type throttled struct {
	time   time.Time
	params map[string]string
}

type throttle struct {
	name      string
	action    operation
	dec       *declaration.ThrottleOperation
	c         *cBuild
	in        chan throttled
	closeChan chan int

	throttleds map[string]throttled
}

func newThrottle(c *cBuild, dec *declaration.ThrottleOperation, name string) *throttle {
	t := &throttle{
		name:       name,
		c:          c,
		dec:        dec,
		closeChan:  make(chan int, 1),
		in:         make(chan throttled),
		throttleds: map[string]throttled{},
	}
	go t.process()

	return t
}

// process will listen for ticks every one tenth of the delay
// it will run the target action with given params when the first
// call is older than delay and remove it.
// it will also listen for the input channel and add the parameters
// to the delayed actions if it's not there already
func (t *throttle) process() {
	// ticker triggers each delay unit 10 times
	ticker := time.NewTicker(time.Duration(time.Millisecond / 5 * time.Duration(t.dec.Delay)))
	for {
		select {
		case <-ticker.C:
			newMap := map[string]throttled{}
			for k, v := range t.throttleds {
				if v.time.Add(time.Duration(time.Millisecond * time.Duration(t.dec.Delay))).After(time.Now()) {
					// deliver the thing
					t.action.run(v.params)
				} else { // keep the thing
					newMap[k] = v
				}
			}
			t.throttleds = newMap
		case <-t.closeChan:
			return
		case td := <-t.in:
			hash := paramsHash(td.params)
			_, has := t.throttleds[hash]
			if has {
				// we have the hash already in the queue, so we can discard it
				continue
			}
			td.time = time.Now()
			t.throttleds[hash] = td
		}
	}
}

func (t *throttle) close() {
	t.closeChan <- 1
}

// run will just add the params into the in queue
func (t *throttle) run(params map[string]string) {
	t.in <- throttled{params: params}
}

// paramsHash creates some kind of hash over a map
// it's probably sufficient for our case but it could create collisions
func paramsHash(m map[string]string) string {
	r := "h:"
	for k, v := range m {
		r += "|" + k + "->" + v
	}
	return r
}
