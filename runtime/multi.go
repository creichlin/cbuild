package runtime

import (
	"gitlab.com/creichlin/cbuild/declaration"
)

type multi struct {
	name    string
	actions []operation
	c       *cBuild
}

func newMulti(c *cBuild, name string, dec *declaration.MultiAction) *multi {
	m := &multi{
		name: name,
		c:    c,
	}
	return m
}

func (m *multi) close() {
}

func (m *multi) run(params map[string]string) {
	for _, a := range m.actions {
		a.run(params)
	}
}
