package runtime

import (
	"fmt"
	"os"
	"reflect"
	"sync"
	"time"

	"gitlab.com/creichlin/cbuild/notifypath"
	"gitlab.com/creichlin/cbuild/report"
)

var defaultOptions = []Option{
	OptReport(report.NewTerminal(10, os.Stdout)),
	OptFileChange(nil),
}

// Option is an interface that implements a runtime option
type Option interface {
	// apply the option to the underlying cBuild
	apply(c *cBuild) error
	// close resources that are optionally created by apply
	close() error
}

func (c *cBuild) applyOptions(options []Option) error {
	// first we will add all default options which types
	// are not yet supplied as option.
	supplied := map[string]bool{}

	for _, option := range options {
		supplied[reflect.TypeOf(option).Elem().Name()] = true
	}

	// we have to store options o we can close them again
	c.options = []Option{}
	for _, def := range defaultOptions {
		if !supplied[reflect.TypeOf(def).Elem().Name()] {
			c.options = append(c.options, def)
		}
	}

	// second add the provided options
	c.options = append(c.options, options...)

	// apply all options, default and supplied
	for _, option := range c.options {
		err := option.apply(c)
		if err != nil {
			return err
		}
	}
	return nil
}

// OptLogging enables logging
func OptLogging(enabled bool) Option {
	return &optLogging{
		enabled: enabled,
	}
}

type optLogging struct {
	enabled bool
}

func (o *optLogging) apply(c *cBuild) error {
	if o.enabled {
		l := &sync.Mutex{}
		c.log = func(msg string, ps ...interface{}) {
			l.Lock()
			defer l.Unlock()
			fmt.Print(time.Now().Format("15:04:05.000") + "   | ")
			fmt.Printf(msg, ps...)
			fmt.Println()
		}
	}
	return nil
}

func (o *optLogging) close() error {
	return nil
}

// OptReport sets the output to the specified Report instance
func OptReport(report report.Report) Option {
	return &optReport{
		report: report,
	}
}

type optReport struct {
	report report.Report
}

func (s *optReport) apply(c *cBuild) error {
	if c.stdout != nil {
		return fmt.Errorf("OptStdout option can only be set once")
	}

	c.stdout = s.report
	return nil
}

func (s *optReport) close() error {
	return nil
}

// OptFileChange will set the given input channel as reader for file changes
// If not set a new one will be created that listens on the current working dir.
// must be created only once
func OptFileChange(fc chan string) Option {
	return &optFileChange{
		fc: fc,
	}
}

type optFileChange struct {
	fc chan string
	nr *notifypath.NotifyRoot
}

func (o *optFileChange) apply(c *cBuild) error {
	if c.fileChanges != nil {
		return fmt.Errorf("file change channel already set, can not set the option twice")
	}

	// if channel is nil we crate a default one
	if o.fc == nil {
		o.fc = make(chan string, 1000)
		wd, err := os.Getwd()
		if err != nil {
			return err
		}
		nr, err := notifypath.NewNotifyRoot(wd, o.fc, c.exclude)
		if err != nil {
			return err
		}
		o.nr = nr
	}
	c.fileChanges = o.fc
	return nil
}

func (o *optFileChange) close() error {
	if o.nr != nil {
		o.nr.Stop()
	}
	return nil
}
