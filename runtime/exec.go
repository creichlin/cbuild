package runtime

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	x "os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/util"
)

type exec struct {
	name string
	dec  *declaration.ExecAction
	c    *cBuild

	command   *template.Template
	args      []*template.Template
	stdinFrom *template.Template
	stdoutTo  *template.Template
}

func newExec(c *cBuild, name string, dec *declaration.ExecAction) (*exec, error) {
	cmd, err := template.New(name).Parse(dec.Command)
	if err != nil {
		return nil, err
	}

	args, err := util.AsTemplateList(name, dec.Args)
	if err != nil {
		return nil, err
	}

	stdinFrom, err := template.New(name).Parse(dec.StdinFrom)
	if err != nil {
		return nil, err
	}

	stdoutTo, err := template.New(name).Parse(dec.StdoutTo)
	if err != nil {
		return nil, err
	}

	return &exec{
		name:      name,
		dec:       dec,
		c:         c,
		command:   cmd,
		args:      args,
		stdinFrom: stdinFrom,
		stdoutTo:  stdoutTo,
	}, nil
}

func (e *exec) run(params map[string]string) {
	e.c.log("exec %v %v", e.name, params)
	c, err := util.EvalTemplate(e.command, params)
	if err != nil {
		log.Println(err)
		return
	}

	args, err := util.EvalTemplateList(e.args, params)
	if err != nil {
		log.Println(err)
		return
	}

	stdinFrom, err := util.EvalTemplate(e.stdinFrom, params)
	if err != nil {
		log.Println(err)
		return
	}
	stdoutTo, err := util.EvalTemplate(e.stdoutTo, params)
	if err != nil {
		log.Println(err)
		return
	}

	cmd := x.Command(c, args...)

	if stdinFrom != "" {
		f, err2 := os.Open(stdinFrom)
		if err2 != nil {
			log.Println(err2)
			return
		}
		cmd.Stdin = f
		defer f.Close() // nolint
	}

	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}
	cmd.Stdout = stdout
	cmd.Stderr = stderr
	err = cmd.Run()
	if err != nil {
		e.c.stdout.Info(e.name, err.Error())
		e.c.stdout.Info(e.name, ">"+c+strings.Join(args, " "))
		e.c.stdout.Info(e.name, stdout.String())
		e.c.stdout.Info(e.name, stderr.String())
		return
	}
	if stdoutTo != "" {
		e.writeStdoutToFile(stdoutTo, stdout.Bytes())
	} else {
		e.c.stdout.Info(e.name, stdout.String())
	}
}

func (e *exec) writeStdoutToFile(stdoutTo string, stdoutBytes []byte) {
	// write stdout to file...
	err := os.MkdirAll(filepath.Dir(stdoutTo), 0755) // nolint: gas
	if err != nil {
		log.Printf("failed to create path to stdout file '%v', %v", stdoutTo, err)
		return
	}

	there, err := ioutil.ReadFile(stdoutTo)
	if err != nil && os.IsNotExist(err) {
		// file doesn't exist, just write it
		err = ioutil.WriteFile(stdoutTo, stdoutBytes, 0644) // nolint: gas
		if err != nil {
			log.Printf("failed to write file '%v', %v", stdoutTo, err)
			return
		}
		e.c.stdout.Info(e.name, fmt.Sprintf("stdout: created %v", stdoutTo))
		return
	}

	// file is already there, compare the content
	if bytes.Equal(there, stdoutBytes) {
		// files are the same, we are done
		e.c.stdout.Info(e.name, fmt.Sprintf("stdout: no change to %v", stdoutTo))
		return
	}

	// content is different...
	err = ioutil.WriteFile(stdoutTo, stdoutBytes, 0644)
	if err != nil {
		log.Printf("failed to write file '%v', %v", stdoutTo, err)
		return
	}
	e.c.stdout.Info(e.name, fmt.Sprintf("stdout: updated %v", stdoutTo))

}

func (e *exec) close() {
	// we don't have any external resources open, so we do nothing
}
