package runtime

import (
	"bytes"
	"fmt"
	"text/template"
)

type logger struct {
	name string
	t    *template.Template
	c    *cBuild
}

func newLogger(c *cBuild, name, message string) (*logger, error) {
	t, err := template.New(name).Parse(message)
	if err != nil {
		return nil, err
	}
	return &logger{
		name: name,
		t:    t,
		c:    c,
	}, nil
}

func (l *logger) run(params map[string]string) {

	o := bytes.NewBufferString("")

	err := l.t.Execute(o, params)
	if err != nil {
		fmt.Fprintf(o, "failed to merge log template for '%v'", l.name)
		return
	}
	l.c.stdout.Info(l.name, o.String())
}

func (l *logger) close() {
	// we don't have any external resources open, so we do nothing
}
