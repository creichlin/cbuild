package runtime

import (
	"bufio"
	"fmt"
	"log"
	"os"
	osexec "os/exec"
	"text/template"
	"time"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/util"
)

type service struct {
	name string
	dec  *declaration.ServiceAction
	c    *cBuild
	cmd  *osexec.Cmd

	command *template.Template
	args    []*template.Template
	env     map[string]*template.Template
}

func newService(c *cBuild, name string, dec *declaration.ServiceAction) (*service, error) {
	cmd, err := template.New(name).Parse(dec.Command)
	if err != nil {
		return nil, err
	}

	args, err := util.AsTemplateList(name, dec.Args)
	if err != nil {
		return nil, err
	}

	env, err := util.AsTemplateMap(name, dec.Env)
	if err != nil {
		return nil, err
	}

	return &service{
		name:    name,
		dec:     dec,
		c:       c,
		command: cmd,
		args:    args,
		env:     env,
	}, nil
}

func (e *service) run(params map[string]string) {
	c, err := util.EvalTemplate(e.command, params)
	if err != nil {
		log.Println(err)
		return
	}

	args, err := util.EvalTemplateList(e.args, params)
	if err != nil {
		log.Println(err)
		return
	}

	env, err := util.EvalTemplateMap(e.env, params)
	if err != nil {
		log.Println(err)
		return
	}

	e.close()

	e.cmd = osexec.Command(c, args...)

	e.cmd.Env = append(e.cmd.Env, os.Environ()...)
	for key, value := range env {
		value = util.EvalEnv(value)
		e.cmd.Env = append(e.cmd.Env, fmt.Sprintf("%v=%v", key, value))
	}

	stdout, err := e.cmd.StdoutPipe()
	if err != nil {
		log.Println(err)
		return
	}
	go func() {
		scanner := bufio.NewScanner(stdout)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			m := scanner.Text()
			e.c.stdout.Info(e.name, m)
		}
	}()

	stderr, err := e.cmd.StderrPipe()
	if err != nil {
		log.Println(err)
		return
	}
	go func() {
		scanner := bufio.NewScanner(stderr)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			m := scanner.Text()
			e.c.stdout.Info(e.name, m)
		}
	}()

	err = e.cmd.Start()
	if err != nil {
		e.c.stdout.Info(e.name, err.Error())
		return
	}
}

func (e *service) close() {
	if e.cmd != nil {
		for i := 0; i < 100; i++ {
			if e.cmd.Process != nil {
				continue
			}
			time.Sleep(time.Millisecond * 10)
		}

		if e.cmd.Process == nil {
			log.Println("process not yet started, can not close")
			return
		}

		err := e.cmd.Process.Signal(os.Interrupt)
		if err != nil {
			e.c.stdout.Info(e.name, err.Error())
			return
		}

		err = e.cmd.Wait()
		if err != nil {
			e.c.stdout.Info(e.name, err.Error())
		}
	}
}
