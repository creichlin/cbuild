package runtime

import (
	"bytes"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"gitlab.com/creichlin/cbuild/declaration"
	"gitlab.com/creichlin/cbuild/report"
)

func TestStdinFromFile(t *testing.T) {
	tf := newTestFixture(t, "tests/exec/stdin_from_file.yaml")
	tf.trigger("test.txt")
	tf.expectOut("exec | 2 foo lala\n     | 4 foo\n")
	tf.close()
}

func TestStdoutFromFile(t *testing.T) {
	tf := newTestFixture(t, "tests/exec/stdout_from_file.yaml")
	tf.trigger("test.txt")
	tf.expectOut("exec | stdout: created tests/tmp/stdout_from_file.txt\n")
	tf.close()

	defer os.Remove("tests/tmp/stdout_from_file.txt") // nolint

	d, err := ioutil.ReadFile("tests/tmp/stdout_from_file.txt")
	if err != nil {
		t.Fatal(err)
	}

	expected := "foo test.txt\n"
	if string(d) != expected {
		t.Errorf("stdout file should have content '%v' but has '%v'", expected, string(d))
	}
}

func TestStdoutFromFileSame(t *testing.T) {
	tf := newTestFixture(t, "tests/exec/stdout_from_file.yaml")
	tf.trigger("test.txt")
	tf.trigger("test.txt")
	tf.expectOut("exec | stdout: created tests/tmp/stdout_from_file.txt\n" +
		"exec | stdout: no change to tests/tmp/stdout_from_file.txt\n")
	tf.close()

	os.Remove("tests/tmp/stdout_from_file.txt") // nolint
}

func TestStdoutFromFileDifferent(t *testing.T) {
	tf := newTestFixture(t, "tests/exec/stdout_from_file.yaml")
	tf.trigger("foo.txt")
	tf.trigger("bar.txt")
	tf.expectOut("exec | stdout: created tests/tmp/stdout_from_file.txt\n" +
		"exec | stdout: updated tests/tmp/stdout_from_file.txt\n")
	tf.close()

	os.Remove("tests/tmp/stdout_from_file.txt") // nolint
}

type testFixture struct {
	t      *testing.T
	r      Runtime
	sender chan string
	out    *bytes.Buffer
}

func newTestFixture(t *testing.T, file string) *testFixture {
	dec := declaration.MustReadDeclarationFile(file)
	sender := make(chan string)
	out := bytes.NewBufferString("")
	r, err := New(dec, []string{}, OptFileChange(sender), OptReport(report.NewPlain(out)))
	if err != nil {
		t.Fatal(err)
	}

	return &testFixture{
		t:      t,
		r:      r,
		sender: sender,
		out:    out,
	}
}

// will send a trigger and sleep for 20 ms, to give the system time to work on it
// and also handle multiple triggers as different events
func (tf *testFixture) trigger(file string) {
	tf.sender <- file
	time.Sleep(time.Millisecond * 20)
}

func (tf *testFixture) expectOut(expected string) {
	o := tf.out.String()
	if o != expected {
		tf.t.Errorf("Output is '%v' instead of '%v'", o, expected)
	}
}

func (tf *testFixture) close() {
	tf.r.Close()
}
