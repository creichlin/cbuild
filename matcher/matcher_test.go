package matcher

import (
	"fmt"
	"testing"
	"time"
)

type tc struct {
	rx    string
	tests map[string]string
}

var cases = []tc{
	{
		rx: `(?P<foo>.*)\.faa\.js$`,
		tests: map[string]string{
			"balbla.faa.js": "balbla.faa.js:balbla.faa.js:foo=balbla",
			"x.faa.js":      "x.faa.js:x.faa.js:foo=x",
		},
	},
	{
		rx: `\.(?P<foo>.*)\.js$`,
		tests: map[string]string{
			"balbla.faa.js": "balbla.faa.js:.faa.js:foo=faa",
			"x.y.js":        "x.y.js:.y.js:foo=y",
		},
	},
}

func TestMatch(t *testing.T) {

	for _, c := range cases {
		t.Run("", func(t *testing.T) {
			sender := make(chan string, 100)
			n := NewMatcher(sender, func(string, ...interface{}) {})

			t1 := make(chan Match, 100)
			err := n.Add(c.rx, t1)
			if err != nil {
				t.Error(err)
			}
			n.Start()

			for source, target := range c.tests {
				t.Run("", func(t *testing.T) {
					sender <- source
					time.Sleep(time.Millisecond * 10)
					res := ""
					fmt.Println(len(t1))
					if len(t1) > 0 {
						res = (<-t1).String()
					}
					if target != res {
						t.Errorf("wrong match:\nexpected: %v\nactual: %v", target, res)
					}
				})
			}
		})
	}

}

func TestMultipleTargets(t *testing.T) {

	sender := make(chan string, 100)

	n := NewMatcher(sender, func(string, ...interface{}) {})

	t1 := make(chan Match, 100)
	n.Add(`/foo/.*/bar`, t1)

	t2 := make(chan Match, 100)
	n.Add(`/foo/zzz/bar`, t2)

	t3 := make(chan Match, 100)
	n.Add(`^/foo/zzz/bar$`, t3)

	n.Start()

	sender <- "/foo/xxx/bar/zur"
	sender <- "/foo/zzz/bar/zur"
	sender <- "/foo/zzz/bar"

	time.Sleep(time.Millisecond)

	l1 := "[/foo/xxx/bar/zur:/foo/xxx/bar /foo/zzz/bar/zur:/foo/zzz/bar /foo/zzz/bar:/foo/zzz/bar]"
	l2 := "[/foo/zzz/bar/zur:/foo/zzz/bar /foo/zzz/bar:/foo/zzz/bar]"
	l3 := "[/foo/zzz/bar:/foo/zzz/bar]"

	r1 := fetch(t1)
	if fmt.Sprint(r1) != l1 {
		t.Errorf("expected: %v\nactual: %v\n", l1, r1)
	}

	r2 := fetch(t2)
	if fmt.Sprint(r2) != l2 {
		t.Errorf("expected: %v\nactual: %v\n", l2, r2)
	}

	r3 := fetch(t3)
	if fmt.Sprint(r3) != l3 {
		t.Errorf("expected: %v\nactual: %v\n", l3, r3)
	}
}

func fetch(s chan Match) []Match {
	r := []Match{}
	for len(s) > 0 {
		x := <-s
		r = append(r, x)
	}
	return r
}
