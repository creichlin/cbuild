package matcher

import (
	"regexp"
)

// Matcher will receive string patterns from a channel
// and match them against a list of regexpes. If they match
// it will be forwarded to a channel associated with that regexp
type Matcher struct {
	source     chan string
	filters    []*filter
	stopSignal chan int
	log        func(string, ...interface{})
	merger     *merger
}

type filter struct {
	rx     *regexp.Regexp
	target chan Match
}

// Match is the type that gets sendt to the target channel if a pattern
// matches
type Match struct {
	Pattern string
	Match   string
	Groups  map[string]string
}

// String will return a string representation of the match of the form:
// {full pattern}:{full match}:{group1=value1}:{group1=value2} ... (with no curly things)
func (m Match) String() string {
	r := m.Pattern + ":" + m.Match
	for k, v := range m.Groups {
		r += ":" + k + "=" + v
	}
	return r
}

// NewMatcher creates a matcher instance using given channel as source
func NewMatcher(source chan string, log func(string, ...interface{})) *Matcher {
	return &Matcher{
		source:     source,
		stopSignal: make(chan int),
		log:        log,
		merger:     newMerger(),
	}
}

// Start will start reading and matching from the source and return
func (m *Matcher) Start() {
	go m.merger.process()
	go m.run()
}

func (m *Matcher) run() {
	for {
		select {
		case p := <-m.source:
			m.process(p)
		case <-m.stopSignal:
			m.merger.close()
			return
		}
	}
}

func (m *Matcher) process(p string) {
	m.log("file changed: %v ", p)
	for _, f := range m.filters {
		matches := f.rx.FindStringSubmatch(p)
		if matches == nil {
			continue
		}

		match := &Match{
			Pattern: p,
			Match:   matches[0],
			Groups:  map[string]string{},
		}

		for i, n := range f.rx.SubexpNames() {
			if n != "" {
				match.Groups[n] = matches[i]
			}
		}
		m.merger.add(match, f)
	}
}

// Add will register a pattern with given channel to forward matches
func (m *Matcher) Add(rx string, target chan Match) error {
	r, err := regexp.Compile(rx)
	if err != nil {
		return err
	}
	m.filters = append(m.filters, &filter{
		rx:     r,
		target: target,
	})
	return nil
}
