package matcher

import (
	"fmt"
	"time"
)

type merger struct {
	matches   map[hash]*mm
	in        chan *mm
	closeChan chan int
}

func newMerger() *merger {
	return &merger{
		matches:   map[hash]*mm{},
		in:        make(chan *mm, 100),
		closeChan: make(chan int),
	}
}

type mm struct {
	match   *Match
	filter  *filter
	expires time.Time
}

type hash struct {
	groups string
	filter *filter
}

func (m *merger) close() {
	m.closeChan <- 0
}

func (m *merger) add(match *Match, filter *filter) {
	m.in <- &mm{
		expires: time.Now().Add(time.Millisecond * 200),
		match:   match,
		filter:  filter,
	}
}

func (m *merger) process() {
	ticker := time.NewTicker(time.Millisecond * 10)
	for {
		select {
		case <-ticker.C:
			for k, v := range m.matches {
				if v.expires.Before(time.Now()) {
					v.filter.target <- *v.match
					delete(m.matches, k)
				}
			}
		case <-m.closeChan:
			return
		case td := <-m.in:
			hash := hash{
				groups: fmt.Sprintf("%v", td.match.Groups),
				filter: td.filter,
			}
			_, has := m.matches[hash]
			if !has {
				m.matches[hash] = td
			}
		}
	}
}
