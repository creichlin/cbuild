package util

import (
	"os"
	"regexp"
)

var envRe = regexp.MustCompile(`\${(.*?)}`)

func EvalEnv(v string) string {
	evaluated := envRe.ReplaceAllStringFunc(v, func(match string) string {
		envVar := match[2 : len(match)-1]
		value, found := os.LookupEnv(envVar)
		if found {
			return value
		}
		return match
	})

	return evaluated
}
