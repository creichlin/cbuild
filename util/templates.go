package util

import (
	"bytes"
	"strconv"
	"text/template"
)

// AsTemplateList will take a list and parse it into a list of gotemplates
// names will be name.i where i is the index in the list
func AsTemplateList(name string, ts []string) ([]*template.Template, error) {
	res := []*template.Template{}
	for i, arg := range ts {
		a, err := template.New(name + "." + strconv.Itoa(i)).Parse(arg)
		if err != nil {
			return nil, err
		}
		res = append(res, a)
	}
	return res, nil
}

func AsTemplateMap(name string, ts map[string]string) (map[string]*template.Template, error) {
	res := map[string]*template.Template{}
	for i, arg := range ts {
		a, err := template.New(name + "." + i).Parse(arg)
		if err != nil {
			return nil, err
		}
		res[i] = a
	}
	return res, nil
}

// EvalTemplateList will evaluate the given template list and return a list of strings
func EvalTemplateList(ts []*template.Template, model interface{}) ([]string, error) {
	res := []string{}
	for _, t := range ts {
		result := bytes.NewBufferString("")
		err := t.Execute(result, model)
		if err != nil {
			return nil, err
		}

		res = append(res, result.String())
	}
	return res, nil
}

// EvalTemplateMap will evaluate the given template map and return a map of strings
func EvalTemplateMap(ts map[string]*template.Template, model interface{}) (map[string]string, error) {
	res := map[string]string{}
	for k, t := range ts {
		result := bytes.NewBufferString("")
		err := t.Execute(result, model)
		if err != nil {
			return nil, err
		}

		res[k] = result.String()
	}
	return res, nil
}

// EvalTemplate will evaluate the given template and return a string
func EvalTemplate(t *template.Template, model interface{}) (string, error) {
	result := bytes.NewBufferString("")
	err := t.Execute(result, model)
	if err != nil {
		return "", err
	}
	return result.String(), nil
}
